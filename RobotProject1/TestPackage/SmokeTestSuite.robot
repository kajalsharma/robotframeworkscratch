*** Settings ***
Library    SeleniumLibrary    
Suite Setup        Log    I am inside Test Suite Setup
Suite Teardown     Log    I am inside Test Suite Teardown
Test Setup         Log    I am inside Test Setup    
Test Teardown      Log    I am inside Test Teardown

Default Tags    SmokeTesting

*** Test Cases ***
TestCase_01-Log the Execution
    [Tags]    SanityTesting
    Log    SmokeTestCases Execution Started

TestCase_02-Basic Search Functionality
    #Create Webdriver    Chrome    executable_path
    Set Tags    RegressionTest
    Open Browser    https://google.com    ${browser}
    Set Browser Implicit Wait    5
    Input Text      name=q                Kajal Sharma    
    Press Keys      name=q                ENTER
    Sleep           2
    #Click Button    name=btnK
    #Sleep    2
    Close Browser
    Log   TestCase_02 executed
    Remove Tags    RegressionTest
 
TestCase_03-Verify Login functionality
    [Documentation]    This is a sample login test
    Open Browser       ${URL}    ${browser}
    Maximize Browser Window
    Set Browser Implicit Wait    5
    LoginKW
    Sleep    2            
    Click Element      id=btnLogin
    Close Browser
    Log    TestCase_03 executed
    Log    This test was executed by %{username} on %{os}
    
*** Variables ***
${URL}            https://opensource-demo.orangehrmlive.com/
${browser}        chrome
@{CREDENTIALS}    Admin    admin123 
&{LOGINDATA}      username=Admin    password=admin123

*** Keywords ***
LoginKW
    Input Text         id=txtUsername    @{CREDENTIALS}[0]                               
    Input Password     id=txtPassword    &{LOGINDATA}[password]